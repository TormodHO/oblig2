# -*- coding: utf-8 -*-
"""
Created on Tue Feb 26 11:34:51 2019

@author: Tormo
"""


#Imports
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import random as rd
from perceptron import Perceptron
from adaline import AdalineGD

from sklearn.linear_model import LogisticRegression as LGR
from matplotlib.colors import ListedColormap
from sklearn.datasets import load_breast_cancer

epoch = 8

#get datasett
datasett = pd.read_csv('demography_data.csv', na_values = '?')
#Clean dataset
datasett.dropna(axis=0, subset = datasett.columns[datasett.columns != "ABOVE50K"])

#traing_set = datasett[datasett['ABOVE50K'] == 1]
dummy_cols = ['WORKCLASS', 'EDUCATION', 'MARITALSTATUS', 'OCCUPATION', 'RELATIONSHIP', 'RACE', 'SEX', 'NATIVECOUNTRY']
dummified_data = pd.get_dummies(datasett, columns=dummy_cols, drop_first=True, dtype=float)

#Test data identified by nan values
test_frame = dummified_data.loc[dummified_data["ABOVE50K"].isna()]
#training data identidied by non-nan values 1 or 0
training_frame = dummified_data[dummified_data["ABOVE50K"] == 1]
training_frame = training_frame.append(dummified_data[dummified_data["ABOVE50K"] == 0]) 


X_sets = []
y_sets = []

for i in range(1, 15):
    temp_set = training_frame.sample(frac=1).reset_index(drop=True)
    X_sets.append(temp_set.loc[:, temp_set.columns != "ABOVE50K"])
    y_sets.append(temp_set.loc[:, temp_set.columns == "ABOVE50K"])

df_x, target_y = X_sets[0],y_sets[0]
X_training_set = df_x[:400]
X_validation_set = df_x[400:500]
y_training_set = target_y[:400]
y_validation_set = target_y[400:500]

ada_error_list = []
ada_error_list_prcnt = []

LGR_error_list = []
LGR_error_list_prcnt = []

for i in range(1,5+1):
    X_subset_training = X_training_set[0:50*i]
    y_subset_training = pd.Series(y_training_set[0:50*i].values.reshape(-1, ))
    print('X')

    X_train_std = (X_subset_training - np.mean(X_training_set, axis = 0))/np.std(X_training_set, axis=0)
    X_validation_std = (X_validation_set - np.mean(X_training_set, axis = 0))/np.std(X_training_set, axis=0, ddof = 0)
    LGR_error_rate = []
    LGR_error_prcnt = []
    
    ada_error_rate = []
    ada_error_prcnt = []
    
    for e in range(1, epoch+1):
        ada = AdalineGD(n_iter=e, eta=0.0001)
        ada.fit(X_train_std, y_subset_training)
        test_result = ada.predict(X_validation_std)
        prediction_list = (test_result == y_validation_set)
        error_count_ada = (prediction_list == False).sum()
        ada_error_rate.append((error_count_ada))
        ada_error_prcnt.append((1 - (error_count_ada)/len(y_validation_set))*100)
    
    ada_error_list.append(ada_error_rate)
    ada_error_list_prcnt.append(ada_error_list_prcnt)

error_frame_ada = pd.DataFrame(ada_error_list_prcnt )

ylabel = ['{} out of 400'.format(v*50) for v in range(1, 5+1)]

plt.subplot(111)
plt.xlabel('Epoch')
plt.ylabel('Training sets')
sns.heatmap(error_frame_ada, yticklabels=ylabel, vmax=100)
plt.title('Accuracy of AdalineGD')
plt.show            
        

    