# -*- coding: utf-8 -*-
"""
Created on Thu Mar  7 18:37:22 2019

@author: Tormo
"""



#Imports
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import random as rd
from perceptron import Perceptron
from adaline import AdalineGD

from sklearn.linear_model import LogisticRegression as LGR
from sklearn.model_selection import train_test_split
from matplotlib.colors import ListedColormap
from sklearn.datasets import load_breast_cancer

def validate_data_shape(X_train_df, y_train_df, X_test_df, y_test_df):
    if (len(X_train_df.columns) != len(X_test_df.columns)):
        raise (ValueError, "There number of columns in train and test is different") 
    
    

#get datasett
datasett = pd.read_csv('demography_data.csv', na_values = '?')
#Clean dataset
datasett.dropna(axis=0, subset = datasett.columns[datasett.columns != "ABOVE50K"])

#traing_set = datasett[datasett['ABOVE50K'] == 1]
dummy_cols = ['WORKCLASS', 'EDUCATION', 'MARITALSTATUS', 'OCCUPATION', 'RELATIONSHIP', 'RACE', 'SEX', 'NATIVECOUNTRY']
dummified_data = pd.get_dummies(datasett, columns=dummy_cols, drop_first=True, dtype=float)

#Test data identified by nan values
test_frame = dummified_data.loc[dummified_data["ABOVE50K"].isna()]
#training data identidied by non-nan values 1 or 0
training_frame = dummified_data[dummified_data["ABOVE50K"] == 1]
training_frame = training_frame.append(dummified_data[dummified_data["ABOVE50K"] == 0])


X_frame = training_frame.loc[:, training_frame.columns != "ABOVE50K"]
y_frame = training_frame["ABOVE50K"]

X_training_sets = []
y_training_sets = []

X_test_sets = []
y_test_sets = []

for i in range(1, 16):
    X_train,X_test, y_train, y_test = train_test_split(X_frame, y_frame, stratify=y_frame)
    
    X_training_sets.append(X_train)
    y_training_sets.append(y_train)
    
    X_test_sets.append(X_test)
    y_test_sets.append(y_test)
    
    validate_data_shape(X_train, y_train, X_test, y_test)
    
    